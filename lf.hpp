#pragma once

#include <string>
#include "CImg/CImg.h"

using namespace cimg_library;

template <typename T>
class lf
{
public:
    lf() : mCImg(){};
    lf(std::string str) : mCImg(str.c_str()){};
    lf(const CImg<T> &image) : mCImg(image){};
    void display() const;
    void display_graph() const;
    void display_lines() const;

private:
    CImg<T> mCImg;
};

template <typename T>
void lf<T>::display() const { mCImg.display(); }

template <typename T>
void lf<T>::display_lines() const
{
    CImg<unsigned char> visu(500, 400, 1, 3, 0);
    const unsigned char red[] = {255, 0, 0}, green[] = {0, 255, 0}, blue[] = {0, 0, 255};

    CImgDisplay main_disp(mCImg, "Click a point"), draw_disp(visu, "Intensity profile");
    while (!main_disp.is_closed() && !draw_disp.is_closed())
    {
        main_disp.wait();
        if (main_disp.button() && main_disp.mouse_y() >= 0)
        {
            const int y = main_disp.mouse_y();
            visu.fill(0).draw_graph(mCImg.get_crop(0, y, 0, 0, mCImg.width() - 1, y, 0, 0), red, 1, 1, 0, 255, 0);
            visu.draw_graph(mCImg.get_crop(0, y, 0, 1, mCImg.width() - 1, y, 0, 1), green, 1, 1, 0, 255, 0);
            visu.draw_graph(mCImg.get_crop(0, y, 0, 2, mCImg.width() - 1, y, 0, 2), blue, 1, 1, 0, 255, 0).display(draw_disp);
        }
    }
};
