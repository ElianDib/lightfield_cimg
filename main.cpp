#include <string>
#define cimg_use_cpp11 1

#include "CImg/CImg.h"
#include "lf.hpp"

using namespace cimg_library;

std::string image_path = R"(E:\Development\Code\lightfield_cpp\standard_test_images\lena_color_512.pnm)";

int main()
{
    lf<unsigned char> lftest(image_path);
    lftest.display();
    return 0;
}